﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{

    [SerializeField] private GameObject mainCamera;
    [SerializeField] private GameObject realMainCamera;
    [SerializeField] private GameObject battleCamera;
    // Start is called before the first frame update
    public static GameManager instance;

    private void Awake()
    {
        if (instance != null && instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            instance = this;
        }
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void ChangeCamera(GameState gameState)
    {
        if (gameState == GameState.Battle)
        {
            mainCamera.SetActive(false);
            battleCamera.SetActive(true);
            realMainCamera.SetActive(false);

        }
        else
        {
            mainCamera.SetActive(true);
            battleCamera.SetActive(false);
            realMainCamera.SetActive(true);
        }

    }
    public void ChangeCamera()
    {
        mainCamera.SetActive(!mainCamera.activeInHierarchy);
        battleCamera.SetActive(!battleCamera.activeInHierarchy);
        realMainCamera.SetActive(!realMainCamera.activeInHierarchy);
    }
}
