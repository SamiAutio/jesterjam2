﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum AttackType
{
    Leaf,
    Fire,
    Water
}

namespace IslandMonsters.Attack
{
    public class Move
    {
        private int baseDamage { get; set; }
        private AttackType type { get; set; }

        public Move(int damage, AttackType attack)
        {
            baseDamage = damage;
            type = attack;
        }

    }
}
