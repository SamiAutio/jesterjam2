﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using IslandMonsters.Attack;

namespace IslandMonsters.Monster
{
    public enum Status
    {
        Calm,
        Nervous,
        Agitated
    }

    [System.Serializable]
    public class Creature : MonoBehaviour
    {
        public virtual string CreatureName { get; set; }
        //public virtual Move[] moves { get; private set; }

        public List<Move> moves = new List<Move>();

        public Sprite creatureSprite;

        public Status status = Status.Agitated;

        public float Distance = 100;

        public int trust = 0;
        public int maxTrust = 100;

        public FruitScriptableObject favoriteFruit;

        private int maxHealth;
        public int MaxHealth
        {
            get { return maxHealth; }
            set { maxHealth = value; }
        }
        private int health;
        public virtual int Health
        {
            get => health;
            set => health = value;
        }
        public int MaxHealt { get; internal set; }

        public void HealHP(int healAmount)
        {
            health = (health + healAmount <= maxHealth) ? health = health + healAmount : health = maxHealth;
        }

        public virtual void Interact()
        {
            Debug.Log("Interact with creature!");
        }
        public void TakeDamage(int amount, string fruit)
        {
            int foodMultiplier = 1;

            if(string.Equals(favoriteFruit.fruitName, fruit))
            {
                foodMultiplier = 2;
            }

            health = (health - amount*foodMultiplier >= 0) ? health - amount * foodMultiplier : 0;

            Debug.Log(health);
            if (health <= maxHealth / 2)
                status = Status.Nervous;

        }
    }
}
