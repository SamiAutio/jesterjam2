﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using TMPro;

public class Player : MonoBehaviour
{

    public Dictionary<string, int> inventory = new Dictionary<string, int>();
    public Dictionary<string, FruitScriptableObject> fruitList = new Dictionary<string, FruitScriptableObject>();
    public TextMeshProUGUI coconutText, paprikaText, mangoText;

    public string selectedItem = "";

    public FruitScriptableObject[] fruitSOs;

    private string[] fruits;

    private int health = 100;

    private void Awake()
    {
        foreach(FruitScriptableObject f in fruitSOs)
        {
            inventory.Add(f.fruitName, 0);
            fruitList.Add(f.fruitName, f);
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Fruit"))
        {

            Fruit currentFruit = collision.GetComponent<Fruit>();
            int currentAmount = 0;

            if(inventory.TryGetValue(currentFruit.fruitData.fruitName, out currentAmount))
            {
                Debug.Log(currentFruit.fruitData.fruitName);
                selectedItem = currentFruit.fruitData.fruitName;
                int newFruitCount = currentAmount + 1;
                inventory[currentFruit.fruitData.fruitName] = newFruitCount;

                if(string.Equals(currentFruit.fruitData.fruitName, "Coconut"))
                {
                    coconutText.text = newFruitCount + " x " + currentFruit.fruitData.fruitName;
                    if(newFruitCount > 1)
                    {
                        coconutText.text += "s";
                    }
                    
                }
                else if (string.Equals(currentFruit.fruitData.fruitName, "Pepper"))
                {
                    paprikaText.text = newFruitCount + " x " + currentFruit.fruitData.fruitName;
                    if (newFruitCount > 1)
                    {
                        paprikaText.text += "s";
                    }
                }
                else if (string.Equals(currentFruit.fruitData.fruitName, "Mango"))
                {
                    paprikaText.text = newFruitCount + " x " + currentFruit.fruitData.fruitName;
                    if (newFruitCount > 1)
                    {
                        mangoText.text += "s";
                    }
                }
            }
            else
            {
                
            }
            UpdateInventoryText();
            Debug.Log(currentFruit.fruitData.fruitName);
            collision.gameObject.SetActive(false);
        }
    }

    public void SelectCoconut(string fruit)
    {
        selectedItem = "Coconut";
    }

    public void SelectMango()
    {
        selectedItem = "Mango";
    }

    public void SelectFruit(string fruit)
    {
        selectedItem = fruit;
    }

    public void UpdateInventoryText()
    {
        foreach(KeyValuePair<string, int> f in inventory)
        {
            if (f.Key.Equals("Coconut"))
            {
                coconutText.text = f.Value + " x " + f.Key;
                if (f.Value > 1)
                {
                    coconutText.text += "s";
                }
            }
            else if (f.Key.Equals("Pepper"))
            {
                paprikaText.text = f.Value + " x " + f.Key;
                if (f.Value > 1)
                {
                    paprikaText.text += "s";
                }
            }
            else if (f.Key.Equals("Mango"))
            {
                mangoText.text = f.Value + " x " + f.Key;
                if (f.Value > 1)
                {
                    mangoText.text += "s";
                }
            }


        }
    }
}
