﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using IslandMonsters.Monster;

public class BlockinCreature : MonoBehaviour
{
    public BoxCollider2D boxTrigger, boxCollider;
    public Transform movementTarget;
    public float speed = 1;
    public Animator animator;

    private bool walking = false;
    

    private WorldMapCreature worldMapCreature;
    // Start is called before the first frame update
    void Start()
    {
        worldMapCreature = GetComponent<WorldMapCreature>();
    }

    // Update is called once per frame
    void Update()
    {
        if(GameStateManager.instance.GetCurrentGameState() == GameState.WorldMap && worldMapCreature.status == Status.Calm)
        {
            transform.position = Vector2.MoveTowards(transform.position, movementTarget.position, speed * Time.deltaTime);
            walking = true;
            animator.SetBool("Walking", true);
        }
        else if (transform.position == movementTarget.position)
        {
            animator.SetBool("Walking", false);
        }
    }

    public void Clamed()
    {

    }
}
