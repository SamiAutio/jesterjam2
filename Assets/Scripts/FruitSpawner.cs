﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FruitSpawner : MonoBehaviour
{

    public GameObject fruitPrefab;

    public GameObject currentFruit;

    private bool spawning = false;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(!currentFruit.activeInHierarchy && !spawning)
        {
            StartCoroutine("SpawnFruit");
        }
    }

    IEnumerator SpawnFruit()
    {
        spawning = true;
        yield return new WaitForSecondsRealtime(10f);
        currentFruit = Instantiate(fruitPrefab, transform.position, Quaternion.identity);
        Debug.Log("Fruit Spawned!");
        spawning = false;
    }
}
