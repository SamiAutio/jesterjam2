﻿using UnityEngine;
using System.Collections;
using IslandMonsters.Attack;

[CreateAssetMenu(fileName = "Creature", menuName = "Creature", order = 1)]
public class CreatureScriptableObject : ScriptableObject
{
    public string creatureName = "New Creature";
    public Color thisColor = Color.white;
    public int maxHealth = 100;
    public Sprite sprite;
    public Vector3[] spawnPoints;
    public GameObject creaturePrefab;
}
