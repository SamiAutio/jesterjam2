﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum GameState
{
    WorldMap,
    Battle,
    Paused
}

public class GameStateManager : MonoBehaviour
{
    private GameState gameSate = GameState.WorldMap;
    private GameState lastGameState = GameState.WorldMap;


    public static GameStateManager instance;

    private void Awake()
    {
        if(instance != null && instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            instance = this;
        }

    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void GoToBattle()
    {
        if(gameSate != GameState.Paused)
            ChangeGameState(GameState.Battle);

        GameManager.instance.ChangeCamera(GameState.Battle);
        //camera.orthographic = true;
    }

    public void GoToWorldMap()
    {
        if (gameSate != GameState.Paused)
            ChangeGameState(GameState.WorldMap);

        GameManager.instance.ChangeCamera(GameState.WorldMap);
        //camera.orthographic = false;
    }

    public void ChangeGameState(GameState newState)
    {
        gameSate = lastGameState = newState;

        if (newState == GameState.Paused)
            Time.timeScale = 0;
        else
            Time.timeScale = 1;
    }

    public void PauseGame()
    {
        if (gameSate != GameState.Paused)
            ChangeGameState(GameState.Paused);
        else
            ChangeGameState(lastGameState);
    }

    public GameState GetCurrentGameState() {
        return gameSate;
    }
}
