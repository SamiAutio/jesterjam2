﻿using UnityEngine;
using System.Collections;
using IslandMonsters.Attack;

[CreateAssetMenu(fileName = "Fruit", menuName = "Fruit", order = 1)]
public class FruitScriptableObject : ScriptableObject
{
    public int ID;
    public string fruitName = "New Fruit";
    public Sprite sprite;
}