﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{

    [SerializeField] private float movementSpeed;
    public float maxSpeed;
    public Animator animator;

    private Rigidbody2D rb2d;
    private float xPosition;
    private float yPosition;
    private float horizontal;
    private float vertical;

    private WorldMapCreature currenTarget = null;

    // Start is called before the first frame update
    void Start()
    {
        rb2d = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
#if UNITY_EDITOR
            // Application.Quit() does not work in the editor so
            // UnityEditor.EditorApplication.isPlaying need to be set to false to end the game
            UnityEditor.EditorApplication.isPlaying = false;
#else
         Application.Quit();
#endif
        }

        if (GameStateManager.instance.GetCurrentGameState() != GameState.WorldMap)
        {
            return;
        }


        horizontal = Input.GetAxisRaw("Horizontal");
        vertical = Input.GetAxisRaw("Vertical");


        animator.SetFloat("horizontal", horizontal);
        animator.SetFloat("vertical", vertical);
        /*
        if (Input.GetAxisRaw("Horizontal") > 0)
        {
            xPosition += movementSpeed;
        }
        else if (Input.GetAxisRaw("Horizontal") < 0)
        {
            xPosition -=  movementSpeed;
        }
        else if(Input.GetAxisRaw("Vertical") > 0)
        {
            yPosition += movementSpeed;
        }
        else if (Input.GetAxisRaw("Vertical") < 0)
        {
            yPosition -= movementSpeed;
        }
        */


        if (Input.GetButtonDown("Jump"))
        {
            if(currenTarget != null)
            {
                currenTarget.StartBattle();
                
            }
            Debug.Log("Interact!");
        }
    }
    private void FixedUpdate()
    {
        if(GameStateManager.instance.GetCurrentGameState() == GameState.WorldMap)
            rb2d.velocity = new Vector2(horizontal * movementSpeed, vertical * movementSpeed);
       // rb2d.MovePosition(new Vector2(xPosition, yPosition) * Time.deltaTime);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Creature")){
            currenTarget = collision.GetComponent<WorldMapCreature>();
            Debug.Log("Enemy!");
        }
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.CompareTag("Creature"))
        {
            currenTarget = null;
            Debug.Log("No target;");
        }
    }
}
