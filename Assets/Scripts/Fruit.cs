﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fruit : MonoBehaviour
{
    public FruitScriptableObject fruitData;
    private void Awake()
    {
        
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void OnPickup()
    {

    }

    public void DestroyThis()
    {
        Destroy(this.gameObject);
    }
    public void FeedAnimation()
    {
        BattleManager.instance.FeedAnimation();
    }
}
