﻿using IslandMonsters.Attack;
using IslandMonsters.Monster;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WorldMapCreature : Creature
{
    public override string CreatureName { get => base.CreatureName; set => base.CreatureName = value; }

    public CreatureScriptableObject creature;

    public Animator animator;

    public Vector2 startPosition;

    // Start is called before the first frame update
    void Start()
    {
        startPosition = transform.position;
        creatureSprite = creature.sprite;
        Health = creature.maxHealth;
        MaxHealth = creature.maxHealth;
        CreatureName = creature.creatureName;
        moves.Add(new Move(2, AttackType.Fire));
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void StartBattle()
    {
        GameStateManager.instance.GoToBattle();
        GameManager.instance.ChangeCamera(GameState.Battle);
        Debug.Log("Fight!");
        BattleManager.instance.StartBattle();
        animator.SetBool("Battle", true);
    }

    public void Attack()
    {
        animator.SetTrigger("Attack");
    }

    public void StopBattle()
    {
        animator.SetBool("Battle", false);
        transform.position = startPosition;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            if (status != Status.Calm)
            {
                BattleManager.instance.SetCurrentCreature(this, this.gameObject);

                
            }
            else
            {
                Debug.Log("You are already friends!");
            }
        }
    }

}
