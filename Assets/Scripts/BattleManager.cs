﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using IslandMonsters.Monster;
using IslandMonsters.Attack;
using TMPro;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class BattleManager : MonoBehaviour
{

    public Creature currentCreature;
    public GameObject targetCreature;
    private Vector3 creatureOldPosition;
    //private GameObject battleCreature;
    public GameObject creaturePosition;

    public int numberOfCreatures;

    public GameObject textObject;

    public GameObject heartsRoot;
    public GameObject heartPrefab;

    public Slider distanceSlider;
    public float sliderSpeed = 2f;

    public Player player;
    public GameObject[] worldMapUI;
    public GameObject petMeUI;

    public ParticleSystem particleSystem;

    public GameObject fruitPrefab;
    public GameObject fruitPosition;

    private AudioSource audioSource;

    private float distance;
    private TextMeshProUGUI text;

    public static BattleManager instance;

    private Animator targetAnimator;

    private void Awake()
    {
        if (instance != null && instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            instance = this;
        }
        player = FindObjectOfType<Player>();
        //targetSpriteRenderer = targetCreature.GetComponent<SpriteRenderer>();
        text = textObject.GetComponent<TextMeshProUGUI>();
        audioSource = GetComponent<AudioSource>();
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        /*if(distance != distanceSlider.value)
        {
            distanceSlider.value = Mathf.MoveTowards(distanceSlider.value, distance, sliderSpeed * Time.deltaTime);
        }*/
        if (currentCreature != null)
        {
            if (currentCreature.Health == 0)
            {
                petMeUI.SetActive(true);
            }
            else
            {
                petMeUI.SetActive(false);
            }
        }
    }

    public void SetCurrentCreature(Creature creature, GameObject target)
    {
        currentCreature = creature;

        targetCreature = target;

        Debug.Log("Current monster: " + creature.CreatureName.ToString());
        text.SetText(creature.CreatureName.ToString());
        // distanceSlider.value = distance = CalculateDistance();
    }

    public void StartBattle()
    {
        foreach (GameObject go in worldMapUI)
        {
            go.SetActive(false);
        }
        creatureOldPosition = targetCreature.transform.position;
        Debug.Log(creatureOldPosition);
        //targetCreature.transform.position = creaturePosition.transform.position;
        targetCreature.transform.position = new Vector3(creaturePosition.transform.position.x, creaturePosition.transform.position.y, 0f);
        targetAnimator = targetCreature.gameObject.GetComponentInChildren<Animator>();
    }

    public void Feed()
    {
        int fruit = 0;
        if (player.inventory.TryGetValue(player.selectedItem, out fruit))
        {
            Debug.Log(fruit);
            Debug.Log(player.selectedItem);
            if (fruit > 0)
            {
                Debug.Log("FEED!");
                currentCreature.TakeDamage(10, player.selectedItem);
                GameObject feedFruit = Instantiate(fruitPrefab, fruitPosition.transform.position, Quaternion.identity, fruitPosition.transform);

                for(int i = currentCreature.MaxHealth; i > (currentCreature.Health); i=i-10)
                {
                    GameObject go = Instantiate(heartPrefab, heartsRoot.transform.position, Quaternion.identity);
                    go.transform.SetParent(heartsRoot.transform);
                } 
                if(currentCreature.Health == 0)
                {
                    particleSystem.Play();
                }
                feedFruit.GetComponent<SpriteRenderer>().sprite = player.fruitList[player.selectedItem].sprite;
                player.inventory[player.selectedItem]--;
            }
            player.UpdateInventoryText();
        }

        
    }

    public void Pet()
    {

        if (currentCreature.Health == 0)
        {
            Debug.Log("You are now friends!");
            audioSource.Play();
            if(currentCreature.status != Status.Calm)
            {
                numberOfCreatures--;
            }
            currentCreature.status = Status.Calm;
            if(numberOfCreatures <= 0)
            {
                SceneManager.LoadScene(2);
            }
            Flee();
        }
        else
        {
            targetAnimator.SetTrigger("Attack");
            Debug.Log("Creature still needs some food!");
        }
    }

    public void Flee()
    {
        if (worldMapUI.Length > 0)
        {
            foreach (GameObject go in worldMapUI)
            {
                go.SetActive(true);
            }
        }

        targetCreature.GetComponent<WorldMapCreature>().StopBattle();

        targetCreature.transform.position = creatureOldPosition;
        Debug.Log(creatureOldPosition);
        GameStateManager.instance.GoToWorldMap();
    }

    public void Approach()
    {
        if (currentCreature.status <= Status.Agitated && currentCreature.status >= Status.Calm)
        {
            if (currentCreature.Distance > 50)
            {
                currentCreature.Distance -= 10;
            }
        } 
        else if (currentCreature.status != Status.Calm)
        {
            if(currentCreature.Distance > 0)
                currentCreature.Distance -= 10;
        }
        distance = CalculateDistance();
    }

    public float CalculateDistance()
    {
        Debug.Log("Current Distance: " + ((100 - currentCreature.Distance) / 100));
        return (100 - currentCreature.Distance) / 100;     
    }

    public void FeedAnimation()
    {
        targetAnimator.SetTrigger("Attack");
    }
}
