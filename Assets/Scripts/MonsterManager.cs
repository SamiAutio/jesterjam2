﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using IslandMonsters.Monster;

public class MonsterManager : MonoBehaviour
{
    public List<CreatureScriptableObject> monsterData = new List<CreatureScriptableObject>();
    public List<Creature> allMonsters = new List<Creature>();

    private Creature[] creatures;


    // Start is called before the first frame update
    void Awake()
    {
        creatures = FindObjectsOfType<Creature>();

        foreach(Creature c in creatures)
        {
            allMonsters.Add(c);
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public bool CheckAllCreatures()
    {
        foreach(Creature c in allMonsters)
        {
            if(c.status != Status.Calm)
            {
                return false;
            }
        }

        return true;
    }
}
